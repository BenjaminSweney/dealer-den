<?php

namespace App\Http\Controllers;

class WelcomeController extends Controller
{
    /**
     * Display application homepage.
     *
     * @return \Illuminate\View\View
     */
    public function index() {
        return \View::make('application');
    }
}
